let tbody = document.querySelector("tbody");
let inputDate = document.querySelector("#inputDate");
let inputType = document.querySelector("#inputType");
let inputMethod = document.querySelector("#inputMethod");
let inputAmount = document.querySelector("#inputAmount");
let total = document.getElementById("total");
let h1 = document.querySelector("h1");
console.log(h1);

document.addEventListener("DOMContentLoaded", () => {
  let text = "Expenses Tracker";
  let color = [
    "c-pink",
    "c-orange",
    "c-green",
    "c-blue",
    "c-dark-blue",
    "c-purple",
  ];
  for (let i = 0; i < text.length; i++) {
    let randomNumber = Math.floor(Math.random() * 6);
    h1.innerHTML += `<span class='${color[randomNumber]}'>${text[i]}</span>`;
  }
});
function date() {
  tbody.innerHTML += `<tr>
    <td>${inputDate.value}</td>
    <td>${inputType.value}</td>
    <td>${inputMethod.value}</td>
    <td onclick="edit(${Number(inputAmount.value)})">&#8369; ${Number(
    inputAmount.value
  )}</td>
  </tr>`;
  total.innerText = Number(inputAmount.value) + Number(total.innerText);
  inputDate.value = "";
  inputType.value = "";
  inputMethod.value = "";
  inputAmount.value = "";
}

function edit(num) {
  let item = (document.querySelector("#editItem").value = num);
}
